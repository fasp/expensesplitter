$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    //$("table tbody tr").find(".add, .edit").toggle();
    var actions = $("table td:last-child").html();
    // Append table with add row form on add new button click
    $(".add-new").click(function () {
        $(this).attr("disabled", "disabled");
        var index = $("table tbody tr:last-child").index();
        var row = '<tr>' +
            '<td><input type="text" class="form-control" name="name" id="name"></td>' +
            '<td><input type="number" min="0.00" step="0.01" class="form-control" name="expenses" id="expenses"></td>' +
            '<td>' + actions + '</td>' +
            '</tr>';
        $("table").append(row);
        $("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });

    // Edit row on edit button click
    $(document).on("click", ".edit", function () {
        $(this).parents("tr").find("td:not(:last-child)").each(function () {
            const $txt = $(this).text()
            $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
        });
        $(this).parents("tr").find(".add, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");
    });

    // Add row on add button click
    $(".calculate").click(function () {
        var $i = 0;
        const $personsExpenses = [];
        $("table > tbody > tr").each(function () {
            //alert($(this).find('td').eq(0).text() + " " + $(this).find('td').eq(1).text() );
            var obj = {
                name: $(this).find('td').eq(0).text(),
                expense: parseFloat($(this).find('td').eq(1).text())
            };
            $personsExpenses[$i++] = obj;
        });
        console.log(JSON.stringify($personsExpenses));
        const $refunds = expensesSplitter($personsExpenses);
        $("#refundsField").text(refundsToText($refunds));
    });

});

// Add row on add button click
$(document).on("click", ".add", function () {
    const $typeText = 'input[type="text"]';
    const $typeNumber = 'input[type="number"]'
    var $type = $typeText;
    const $selector = $(this);
    //  addEntry($selector, $type)
    //  addEntry($selector, $typeNumber)

    var empty = false;
    var input = $(this).parents("tr").find($type);
    var $val = $(this).val();
    input.each(function () {
        if (!$(this).val()) {
            $(this).addClass("error");
            empty = true;
        } else {
            $(this).removeClass("error");
        }
    });
    var input = $(this).parents("tr").find($type);
    $val = $(this).val();
    $(this).parents("tr").find(".error").first().focus();
    if (!empty) {
        input.each(function () {
            const $val = $(this).val()
            $(this).parent("td").html($(this).val());
        });
        $(this).parents("tr").find(".add, .edit").toggle();
        $(".add-new").removeAttr("disabled");
    }
    ////////////////////////////////////////////
    $type = $typeNumber;
    var empty = false;
    var input = $(this).parents("tr").find($type);
    var $val = $(this).val();
    input.each(function () {
        if (!$(this).val()) {
            $(this).addClass("error");
            empty = true;
        } else {
            $(this).removeClass("error");
        }
    });
    var input = $(this).parents("tr").find($type);
    $val = $(this).val();
    $(this).parents("tr").find(".error").first().focus();
    if (!empty) {
        input.each(function () {
            const $val = $(this).val()
            $(this).parent("td").html($(this).val());
        });
        $(this).parents("tr").find(".add, .edit").toggle();
        $(this).parents("tr").find(".add, .edit").toggle();
        $(".add-new").removeAttr("disabled");
    }
});

function addEntry(selector, type) {
    var empty = false;
    var input = selector.parents("tr").find(type);
    //var $val = selector.val();
    input.each(function () {
        if (!selector.val()) {
            selector.addClass("error");
            empty = true;
        } else {
            selector.removeClass("error");
        }
    });
    var input = selector.parents("tr").find(type);
    $val = selector.val();
    selector.parents("tr").find(".error").first().focus();
    if (!empty) {
        input.each(function () {
            const $val = selector.val()
            selector.parent("td").html(selector.val());
        });
        selector.parents("tr").find(".add, .edit").toggle();
        $(".add-new").removeAttr("disabled");
    }
}

// Delete row on delete button click
$(document).on("click", ".delete", function () {
    $(this).parents("tr").remove();
    $(".add-new").removeAttr("disabled");
});


function expensesSplitter(expensesPersons) {
    expensesPersons.sort(function (a, b) {
        return a.expense - b.expense;
    });
    var allDebts = [];
    var n = expensesPersons.length;
    for (var i = 0; i < n - 1; i++) {
        var factor = (n - (i + 1)) * 1 / n;
        var expensePerson = expensesPersons[i];
        var nextExpensePerson = expensesPersons[i + 1];
        var allDebt = expensesPersons.slice(i + 1, n).map(function (p) {
            return p.expense;
        })
            .reduce(function (acc, val) {
                return acc + val;
            }, 0) / n * (i + 1);
        var receives = expensePerson.expense * factor;
        var refund = allDebt - receives;
        var refundNames = [expensePerson.name, nextExpensePerson.name];
        var entry = [refundNames, refund];
        allDebts.push(entry);
    }
    return allDebts;
}

function refundsToText(refunds) {
    var refundsText = "";
    for (let entry of refunds) {
        let persons = entry[0];
        refundsText += persons[0] + " pays " + persons[1] + " " + entry[1].toFixed(2) + "€.\n"
    }
    return refundsText;
}